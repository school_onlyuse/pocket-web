import flask, configparser, json
from flask_wtf.csrf import *
from libpocket import Oauth, Apis
app = flask.Flask(__name__)

csrf = CsrfProtect(app)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?R'


@app.route('/top')
def top():
	try:
		j = open('consumer_key.json', 'r')
	except FileNotFoundError:
		print("consumer_key.json not found")
		return "consumer_key.json not found"
	except OSError:
		print("error")
		return "error"
	try:
		jc = json.load(j)
	except ValueError:
		print("consumer_key.json Invalid")
		return "consumer_key.json Invalid"
	consumer_key = jc['consumer_key']
	auth = Oauth(consumer_key)
	if 'access_token' and 'username' in flask.session and auth.checkResponse(flask.session['access_token'])['status'] == True:
		flask.session['token'] = generate_csrf()
		return flask.render_template('index.html', token = flask.session['token'])
	else:
		if not ('username' and 'access_token') in flask.session:
			if 'username' in flask.session:
				del flask.session['username']
			if 'access_token' in flask.session:
				del flask.session['access_token']
		callback_uri = flask.request.base_url
		if 'req_code' in flask.session:
			req_code = flask.session['req_code']
			del flask.session['req_code']
			at = auth.getAccessToken(req_code)
			if at['status'] == True:
				flask.session['access_token'] = at['access_token']
				flask.session['username'] = at['username']
				flask.session['token'] = generate_csrf()
				return flask.render_template('index.html', token = flask.session['token'])
			else:
				return 'cannnot get access token'
		else:
			code = auth.getReqCode(callback_uri)
			if code['status'] == True:
				'''
				if not 'code' in code:
					return '<p>error</p>'
				else:
				'''
				flask.session['req_code'] = code['code']
				return '<a href ="'+ auth.getAuthRedirectUrl(code['code'], callback_uri) + '">認証</a>'
			else:
				return 'cannnot get request token'



@csrf.exempt
@app.route('/retrieve', methods = ['POST']) #event listener write here
def eventListenerRetrieve():
	ck = RequestCheck()
	ret = ck.reqCheck(flask.request.headers, flask.session['token'])
	if ret['status'] == False:
		del ret['status']
		return flask.jsonify(ResultSet = ret), 400
	else:
		#CSRF Check ok
		try:
			j = open('consumer_key.json', 'r')
		except FileNotFoundError:
			print("consumer_key.json not found")
			return "consumer_key.json not found"
		except OSError:
			print("error")
			return "error"
		try:
			jc = json.load(j)
		except ValueError:
			print("consumer_key.json Invalid")
			return "consumer_key.json Invalid"
		consumer_key = jc['consumer_key']
		auth = Oauth(consumer_key)
		api = Apis(consumer_key, flask.session['access_token'])
		req_data = flask.request.json
		arg_dict = {
			'sort': 'newest',
			'detailType': 'complete',
			'count': 20
		}
		if 'offset' in req_data:
			arg_dict['offset'] = req_data['offset']
		if 'favorite' in req_data:
			arg_dict['favorite'] = req_data['favorite']
		if 'state' in req_data:
			arg_dict['state'] = req_data['state']
		if 'search' in req_data:
			arg_dict['search'] = req_data['search']
		lists = api.callRetrieve(arg_dict)
		if lists['status'] == False:
			#api return to error
			del lists['status']
			return flask.jsonify(ResultSet = lists), 400
		else:
			#return api response
			del lists['status']
			return_obj = {}
			for key in lists:

				return_obj[key] = {
					'item_id': lists[key]['item_id'],
					'favorite': lists[key]['favorite'],
					'status': lists[key]['status'],
					'excerpt': lists[key]['excerpt'],
					'has_video': lists[key]['has_video'],
					'has_image': lists[key]['has_image']
				}

				if not 'given_url' in lists[key] or lists[key]['given_url'] == '':
					return_obj[key]['url'] = lists[key]['resolved_url']
				else:
					return_obj[key]['url'] = lists[key]['given_url']


				if not 'given_title' in lists[key] or lists[key]['given_title'] == '':
					return_obj[key]['title'] = lists[key]['resolved_title']
				else:
					return_obj[key]['title'] = lists[key]['given_title']

				if int(return_obj[key]['has_video']) == 1:
					return_obj[key]['video'] = {
					'src': lists[key]['videos'][str(1)]['src'],
					'width': lists[key]['videos'][str(1)]['width'],
					'height': lists[key]['videos'][str(1)]['height'],
				}

				if int(return_obj[key]['has_image']) == 1:
					return_obj[key]['image'] = {
					'src': lists[key]['images'][str(1)]['src'],
					'width': lists[key]['images'][str(1)]['width'],
					'height': lists[key]['images'][str(1)]['height'],
				}

			return flask.jsonify(ResultSet = return_obj)


@csrf.exempt
@app.route('/add', methods = ['POST']) #event listener write here
def eventListenerAdd():
	ck = RequestCheck()
	ret = ck.reqCheck(flask.request.headers, flask.session['token'])
	if ret['status'] == False:
		del ret['status']
		return flask.jsonify(ResultSet = ret), 400
	else:
		if ret['status'] == False:
			#header is invaild
			del ret['status']
			return flask.jsonify(ResultSet = ret), 400
		else:
			#send to api request
			cp = configparser.ConfigParser()
			cp.read('consumer_key.ini')
			try:
				j = open('consumer_key.json', 'r')
			except FileNotFoundError:
				print("consumer_key.json not found")
				return "consumer_key.json not found"
			except OSError:
				print("error")
				return "error"
			try:
				jc = json.load(j)
			except ValueError:
				print("consumer_key.json Invalid")
				return "consumer_key.json Invalid"
			consumer_key = jc['consumer_key']
			api = Apis(consumer_key, flask.session['access_token'])
			arg_dict = {}
			req = flask.request.json
			if 'url' in req:
				arg_dict['url'] = req['url']
			else:
				return flask.jsonify(ResultSet = {"msg": "URL Invalid"}), 400
			if 'title' in req:
				arg_dict['title'] = req['title']
			if 'tags' in req:
				arg_dict['tags'] = req['tags']
			if 'tweet_id' in req:
				arg_dict['tweet_id'] = req['tweet_id']
			lists = api.callAdd(arg_dict)
			if lists['status'] == False:
				# api return to error
				del lists['status']
				return flask.jsonify(ResultSet = lists), 400
			else:
				#return api response
				del lists['status']
				return flask.jsonify(ResultSet = lists)



@csrf.exempt
@app.route('/modify', methods = ['POST']) #event listener write here
def eventListenerModify():
	ck = RequestCheck()
	ret = ck.reqCheck(flask.request.headers, flask.session['token'])
	if ret['status'] == False:
		del ret['status']
		return flask.jsonify(ResultSet = ret), 400
	else:
		ret = ck.reqCheck(flask.request.headers, flask.session['token'])
		if ret['status'] == False:
			del ret['status']
			#header is invaild
			return flask.jsonify(ResultSet = ret), 400
		elif not 'method' in flask.request.json:

			return flask.jsonify(ResultSet = {
				'error': '\"method\" key not found'
			}), 400
		elif not flask.request.json['method'] == 'modify':
			return flask.jsonify(ResultSet = {
				'error': '\"method\" value invaild'
			}), 400
			#send to api request
		else:
			try:
				j = open('consumer_key.json', 'r')
			except FileNotFoundError:
				print("consumer_key.json not found")
				return "consumer_key.json not found"
			except OSError:
				print("error")
				return "error"
			try:
				jc = json.load(j)
			except ValueError:
				print("consumer_key.json Invalid")
				return "consumer_key.json Invalid"
			consumer_key = jc['consumer_key']
			json_obj = flask.request.json
			del json_obj['method']
			api = Apis(consumer_key, flask.session['access_token'])
			lists = api.callModify(json_obj)
			if lists['status'] == False:
				del lists['status']
				return flask.jsonify(ResultSet = lists), 400
			else:
				del lists['status']
				return flask.jsonify(ResultSet = lists)



class RequestCheck:
	def reqCheck(self, headers, token):
		if not 'Content-Type' in headers:
			#contentType is not "application/json"
			json_obj = {
				'status': False,
				'msg': 'HTTP Header not found'
			}
			return json_obj
		if not headers['Content-Type'] == 'application/json':
			#contentType is not "application/json"
			json_obj = {
				'status': False,
				'msg': 'HTTP Header to Invaild'
			}
			return json_obj

		if not 'X-CSRFToken' in headers:
			json_obj = {
				'status': False,
				'msg': 'Session header not found'
			}
			return json_obj
		if not token == headers['X-CSRFToken']:
			#CSRF token is invaild
			json_obj = {
				'status': False,
				'msg': 'Session to Invaild'
			}
			return json_obj
		return {'status': True, 'msg': 'header check is ok'}
