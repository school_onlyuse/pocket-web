import requests, json, requests.exceptions

class Oauth:
	CONSUMER_KEY = ''
	def __init__(self, consumer_key):
		self.CONSUMER_KEY = consumer_key

	def getReqCode(self, callback): #request the "request token"
		url = 'https://getpocket.com/v3/oauth/request'
		data = {
			'consumer_key' : self.CONSUMER_KEY,
			'redirect_uri' : callback
		}
		header = {
			'X-Accept' : 'application/json',
			'Content-Type': 'application/json',
			'dataType': 'json'
		}
		try:
			response = requests.post(url, data = json.dumps(data), headers = header)
		except requests.exceptions.RequestException as e:
			return_response =  {'status': False, 'msg': 'Cannot get request code'}
		else:
			if not response.status_code == 200:
				return_response = {'status': False, 'msg': response.headers['x-error']}
			else:
				return_response = response.json()
				return_response['status'] = True
		return return_response # return dicts


	def getAuthRedirectUrl(self, req_code, callback): #generate redirect uri
			return 'https://getpocket.com/auth/authorize?request_token=' + req_code + '&redirect_uri=' + callback


	def getAccessToken(self, req_code): #request the access token
		url = 'https://getpocket.com/v3/oauth/authorize'
		data = {
			'consumer_key' : self.CONSUMER_KEY,
			'code' : req_code
		}
		header = {
			'X-Accept' : 'application/json',
			'Content-Type': 'application/json',
			'dataType': 'json'
		}
		try:
			response = requests.post(url, data = json.dumps(data), headers = header)
		except requests.exceptions.RequestException as e:
			return_response = {'status': False, 'msg' : 'Cannot get Auth info'}
		else:
			if not response.status_code == 200:
				return_response = {'status': False, 'msg': response.headers['x-error']}
			else:
				return_response = response.json()
				return_response['status'] = True

		return return_response # return dicts


	def checkResponse(self, access_token): # access token valid check
		url = 'https://getpocket.com/v3/get'
		data = {
			'consumer_key' : self.CONSUMER_KEY,
			'access_token' : access_token,
			'detailType' : 'simple',
			'count' : 1
		}
		status = True
		code = 200
		header = {
			'Content-Type': 'application/json',
			'dataType': 'json'
		}
		try:
			response = requests.post(url, data = json.dumps(data), headers = header)
		except requests.exceptions.RequestException as e:
			code = -1
			status = False
		if not response.status_code == 200:
			code = response.status_code
			status = False
		return {'status': status, 'status_code': code} #return dict


class Apis:
	CONSUMER_KEY = '' #set consumer key in class variable

	def __init__(self, consumer_key, access_token):
		self.ACCESS_TOKEN = access_token #set access token in instance variable
		self.CONSUMER_KEY = consumer_key

	def callRetrieve(self, data): # call api to retrieve method
		if not 'offset' in data or 'offset' in data and not isinstance(data['offset'], int):
			return_response = {
				'status': False,
				'msg': 'required parameter is invaild'
			}
			return return_response
		else:
			url = 'https://getpocket.com/v3/get'
			required_param = {
				'consumer_key' : self.CONSUMER_KEY,
				'access_token' : self.ACCESS_TOKEN
			}
			required_param.update(data);
			header = {
				'Content-Type': 'application/json',
				'dataType': 'json'
			}
			try:
				response = requests.post(url, data = json.dumps(required_param), headers = header)
			except requests.exceptions.RequestException as e:
				return_response = {'status': False, 'msg': 'request error'}
			else:
				if not response.status_code == 200:
					return_response = {'status':False, 'msg': response.headers['x-error']}
				else:
					return_response = response.json()['list']
					if return_response == []:
						return_response = {'status': True}
					else:
						return_response['status'] = True
				return return_response # return dicts (response is json Strings)


	def callModify(self, data): #call api to modify method
		method_list = {"archive", "readd", "favorite","unfavorite", "delete", "tags_add",
			"tags_remove", "tags_replace", "tags_clear", "tag_rename"}
		if not 'method' or not 'item_id' in data :
			# data['method'] or data['item_id'] is undefined
			return_response = {'status': False, 'msg': 'Invalid keys'}
		elif data['item_id'] == '' or not data['action'] in method_list:
			# data['method'] or data['item_id'] is undefined is null
			return_response = {'status': False, 'msg': 'parameter is null'}
		else:
			url = 'https://getpocket.com/v3/send'
			data['item_id'] = int(data['item_id'])
			required_param = {
				'consumer_key' : self.CONSUMER_KEY,
				'access_token' : self.ACCESS_TOKEN,
				'actions' :[data]
			}
			header = {
				'Content-Type': 'application/json',
				'dataType': 'json'
			}
			try:
				response = requests.post(url, data = json.dumps(required_param), headers = header)
			except requests.exceptions.RequestException as e:
				return_response = {'status': False, 'msg' :'request error'}
			else:
				if not response.status_code == 200:
					return_response = {'status': False, 'msg': response.headers['x-error']}
				else:
					return_response = {'status': True}
			return return_response # return dicts (response is json Strings)


	def callAdd(self, data): #call api to add method
		url = 'https://getpocket.com/v3/add'
		if not 'url' in data:
			#url unspecified
			return {
				'status': False,
				'msg': 'url is unspecified'
			}
		else:
			#url valid
			required_param = {
				'consumer_key' : self.CONSUMER_KEY,
				'access_token' : self.ACCESS_TOKEN,
			}
			required_param.update(data);
			header = {
				'Content-Type': 'application/json',
				'dataType': 'json'
			}
			try:
				response = requests.post(url, data = json.dumps(required_param), headers = header)
			except requests.exceptions.RequestException as e:
				return_response = {'status': False, 'msg': 'request error'}
			else:
				if not response.status_code == 200:
					return_response = {'status': False, 'msg': response.headers['x-error']}
				else:
					return_response = {'status': True}
			return return_response # return dicts (response is json Strings)
