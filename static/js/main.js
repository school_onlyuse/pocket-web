Vue.config.delimiters = ['(%', '%)'];
var tk = token
//ajax send based object
var AJAX_BASE = {
    data:{
        d: {},
        post:{},
        result: {},
        flag: false,
        uri: ""
    },
    methods:{
        ajaxPost: function(){
            $.ajaxSetup({
				beforeSend: function(xhr, settings){
			        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain){
						xhr.setRequestHeader("X-CSRFToken", tk);
					}
				}
			});
            this.result = {}
            var d = new $.Deferred();
            this.d = d
            var pthis = this
            $.ajax({
                type: "post",
                url: this.uri,
                data: JSON.stringify(this.post),
                contentType: "application/json",
                dataType: "json"
            })
            .done(function(res, text_status, xhr){
                if(xhr.status == 200 && !res.error){
                    pthis.flag = true
                    pthis.result = res.ResultSet
                }
                pthis.d.resolve();
            });
            return this.d.promise();
        }
    }
};

//data object(binding to #queries)
var queryvm = new Vue({
    el: '#queries',
    data: {
        count: 0,
        late_post_data: {},
        load_btn_flg: false,
        query: {
            loading_data: {
                title: "loding",
                excerpt: "loding",
                image: {
                    src: "/static/0142.jpg"
                }
            }
        }
    },
    created: function(){
        var ajax_post = new Vue(AJAX_BASE);
        ajax_post.uri = '/retrieve'
        ajax_post.post = {
            count: 20,
            state: "all",
            offset: 0
        };
        var pthis = this;
        ajax_post.ajaxPost().done(function(){
            if(ajax_post.flag){
                Object.keys(ajax_post.result).forEach(function(key, index){
                    if(index == 0)pthis.query = {}
                    pthis.count++
                    ajax_post.result[key].item_id = Number(ajax_post.result[key].item_id)
                    pthis.query.$add(key, ajax_post.result[key])
                })
            }
            pthis.load_btn_flg = true
            $('#contents > .loader').fadeOut('normal', function(){
                $('#queries').fadeIn('normal')
            })
            ajax_post = null
            pthis = null
        });
    },
    methods:{
        queryStrage: function(isnew, option){
            //isnew: boolean , option: Object
            //type true: new condition query , false:add query to before condition
            /*
            option = {
                state: Archive(Archive items only) unArchive(no Archive items only) all items getting to this parameter is undefine',
                favrite: '0(unfavrite only) or 1(favorite only) or undefine(all items)',
                search: 'search string or undefine'
            }
            */
            this.load_btn_flg = false
            var opt = {}
            if(isnew){
                opt = option || {}
            }else{
                opt = this.late_post_data
            }
            this.late_post_data = $.extend(true, {}, opt) //deep copy
            var change_query_type = isnew;
            var ajax_post = new Vue(AJAX_BASE);
            if(isnew){
                this.count = 0
            }
            ajax_post.uri = '/retrieve';
            ajax_post.post = opt
            ajax_post.post.offset = this.count
            var pthis = this;
            ajax_post.ajaxPost().done(function(){
                if(ajax_post.flag){
                    pthis.flag = ajax_post.flag
                    Object.keys(ajax_post.result).forEach(function(key, index){
                        if(index == 0 && change_query_type)pthis.query = {}
                        pthis.count++
                        ajax_post.result[key].item_id = Number(ajax_post.result[key].item_id)
                        pthis.query.$add(key, ajax_post.result[key])
                    })
                }
                pthis.load_btn_flg = true
                ajax_post = null
                pthis = null
            });
        },

        retrieve_new: function(){
            this.queryStrage(true)
        },

        add_query: function(){
            this.queryStrage(false)
        },

        mod_favorite: function(id){
            var ajax_post = new Vue(AJAX_BASE);
            var pid = id
            var obj = {
                method: 'modify',
                item_id: pid
            }
            if (this.query[pid].favorite === '1') {
                obj.action = "unfavorite"
            }else{
                obj.action = "favorite"
            }
            ajax_post.uri = '/modify';
            ajax_post.post = obj
            var pthis = this;
            ajax_post.ajaxPost().done(function(){
                if(ajax_post.flag){
                    if(pthis.query[pid].favorite === '1'){
                        pthis.query[pid].favorite = '0'
                    }else{
                        pthis.query[pid].favorite = '1'
                    }
                }
                pid = null
                obj = null
                ajax_post = null
                pthis = null
            })
        },

        mod_archive: function(id){
            var ajax_post = new Vue(AJAX_BASE);
            var obj = {method: 'modify'}
            var pid = id
            obj.item_id = pid
            if (this.query[id].status === '1') {
                obj.action = "readd"
            }else{
                obj.action = "archive"
            }
            ajax_post.uri = '/modify';
            ajax_post.post = obj
            var pthis = this;
            ajax_post.ajaxPost().done(function(){
                if(ajax_post.flag){
                    if(pthis.query[pid].status === '1'){
                        pthis.query[pid].status = '0'
                    }else{
                        pthis.query[pid].status = '1'
                    }
                }
                pid = null
                obj = null
                ajax_post = null
                pthis = null
            })
        },

        mod_delete: function(id){
            var ajax_post = new Vue(AJAX_BASE);
            var obj = {method: 'modify'}
            var pid = id
            obj.item_id = pid
            obj.action = 'delete'
            ajax_post.uri = '/modify';
            ajax_post.post = obj
            var pthis = this;
            ajax_post.ajaxPost().done(function(){
                if(ajax_post.flag){
                    pthis.count--
                    pthis.query.$delete(pid)
                }
                pid = null
                obj = null
                ajax_post = null
                pthis = null
            })
        }

    }
});


//view model(VM)
action = new Vue({
    el: '#header',
    data: {
        search_info: {
            search: '',
            archive: false,
            favorite: false
        },
        add_info: {
            url: ''
        }
    },
    methods: {
        q_arc: function(){
            queryvm.queryStrage(true, {
                state: 'archive'
            })
        },

        q_fav: function(){
            queryvm.queryStrage(true, {
                favorite: 1
            })
        },

        q_search: function(){
            var obj = {}
            obj.search = this.search_info.search
            if(this.search_info.archive)obj.state = 'archive'
            if(this.search_info.favorite)obj.favorite = 1
            queryvm.queryStrage(true, obj)
            this.search_info.search = ''
            this.search_info.archive = false
            this.search_info.favorite = false
            $('#search-modal').modal('hide')
        },

        q_add: function(){
            var ajax_post = new Vue(AJAX_BASE);
            obj = {
                url: this.add_info.url
            }
            ajax_post.uri = '/add';
            ajax_post.post = obj
            var pthis = this;
            $('#add-modal').modal('hide')
            ajax_post.ajaxPost().done(function(){
                if(ajax_post.flag){
                    $('#add-notification').modal('show')
                }
                pid = null
                obj = null
                ajax_post = null
                pthis = null
            })
        }
    }
});
