## What's this?
勉強のために書いたPocketのWebクライアント。

## Installing
1.クローンしたフォルダの中にある、consumer_key.json.sampleをcomsumer_keyにリネームする<br>
2.上記ファイルを開き、consumer_keyの項目にPocket Developerから取得したコンシューマーキーを入力する<br>
3.Python3 Nginx pipをインストール<br>
4.pip install　uwsgi flask requests flask_wtfを実行<br>
5.Nginxでserver欄にportとservernameを入力した後、<br>
location / {<br>
    include uwsgi_params;<br>
    uwsgi_pass 127.0.0.1:3031;<br>
}<br>
を入力<br>

6.main.pyがあるフォルダで<br>
uwsgi --socket 127.0.0.1:3031 --wsgi-file main.py --callable app --processes 4 --threads 2 &<br>
を実行。<br>
7.Nginxを再起動<br>
8.{servername or IP address}:{port number}/topにアクセス<br>

## use libraries
[Vue.js](http://vuejs.org/)<br>
[jQuery](https://jquery.com/)<br>
[Bootstrap](http://getbootstrap.com/)<br>

## License
MIT<br>
